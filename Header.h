#ifndef header
#define HEADER

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>


typedef struct students {
	char name[21];
	char surname[21];
	int grades[31];
	int numOfGrades;
	
}STUDENT;

typedef struct professors {
	char username[31];
	char password[21];
}PROFESSOR;

STUDENT student;
PROFESSOR professor;

void choice1();
void login();
void signup();
void end();

void createFile(char*);

void subjectChoice();
void menu(FILE*);
void viewStudents(FILE*);
void addingStudents(FILE*);
void addingGrades(FILE*);
void viewGrades(FILE*);
void sortAZ(STUDENT*, int);
void search(FILE*);

void createMzr2(void);
void createOe();
void createPk();
void createProg2();
void createSj2();
void createTk2();


#endif // header